package com.pvt.test;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jiona
 * Date: 22.02.14
 * Time: 6:06
 * To change this template use File | Settings | File Templates.
 */
public class PathFinder {

    private final Labyrinth labyrinth;
    private final Point startPoint;


    public PathFinder(Labyrinth labyrinth, Point startPoint) {
        this.labyrinth = labyrinth;
        this.startPoint = startPoint;
    }

    /**
     * Finds path from start point to end point or return null if there is no path
     *
     * @param endPoint the end point
     * @return the path or null if path doesn't exists
     */
    public List<Point> findPath(Point endPoint) {
        int stepsCounter = 1;
        Map <Point, Integer> KolichestvoShagov = new HashMap<>();
        KolichestvoShagov.put(this.startPoint, 1);
        int k = 1;
        while (!KolichestvoShagov.containsKey(endPoint) && k > 0) {
            k=0;
            Map  <Point, Integer> Cloned = new HashMap<Point, Integer>(KolichestvoShagov);
            for (Map.Entry<Point, Integer> entry : Cloned.entrySet()) {
                if (entry.getValue() == stepsCounter) {
                    int i = entry.getKey().getI();
                    int j = entry.getKey().getJ();
                    if (labyrinth.isRoom(i+1, j) && !KolichestvoShagov.containsKey(new Point(i+1, j))) {
                        KolichestvoShagov.put(new Point(i+1, j), stepsCounter + 1);
                        k++;
                    }
                    if (labyrinth.isRoom(i-1, j) && !KolichestvoShagov.containsKey(new Point(i-1, j))) {
                        KolichestvoShagov.put(new Point(i-1, j), stepsCounter + 1);
                        k++;
                    }
                    if (labyrinth.isRoom(i, j+1) && !KolichestvoShagov.containsKey(new Point(i, j+1))) {
                        KolichestvoShagov.put(new Point(i, j+1), stepsCounter + 1);
                        k++;
                    }
                    if (labyrinth.isRoom(i, j-1) && !KolichestvoShagov.containsKey(new Point(i, j-1))) {
                        KolichestvoShagov.put(new Point(i, j-1), stepsCounter + 1);
                        k++;
                    }
                }
            }
            stepsCounter++;
        }

        if (k == 0) return null;

        List<Point> path = new ArrayList<Point>();
        path.add(endPoint);
        int pathCounter = stepsCounter;
        while (path.size()<stepsCounter) {
            int i = path.get(path.size()-1).getI();
            int j = path.get(path.size()-1).getJ();
            if (labyrinth.isRoom(i+1, j) && KolichestvoShagov.containsKey(new Point(i+1, j)) && KolichestvoShagov.get(new Point(i+1, j)) == pathCounter-1) {
                path.add(new Point(i+1, j));
            } else if (labyrinth.isRoom(i-1, j) && KolichestvoShagov.containsKey(new Point(i-1, j)) && KolichestvoShagov.get(new Point(i-1, j)) == pathCounter-1) {
                path.add(new Point(i-1, j));
            } else if (labyrinth.isRoom(i, j+1) && KolichestvoShagov.containsKey(new Point(i, j+1)) && KolichestvoShagov.get(new Point(i, j+1)) == pathCounter-1) {
                path.add(new Point(i, j+1));
            } else if (labyrinth.isRoom(i, j-1) && KolichestvoShagov.containsKey(new Point(i, j-1)) && KolichestvoShagov.get(new Point(i, j-1)) == pathCounter-1) {
                path.add(new Point(i, j-1));
            }
            pathCounter--;
        }
       Collections.reverse(path);
        return path;
    }

    /**
     * Checks whether path from start point to end point exists
     *
     * @param endPoint the end point
     * @return flag is path exists or not
     */
    public boolean isPathExists(Point endPoint) {
        if (findPath(endPoint) == null) return false;
        else return true;
    }

}
